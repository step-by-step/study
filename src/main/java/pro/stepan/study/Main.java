package pro.stepan.study;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
public class Main {
    @Getter
    private static ClassPathXmlApplicationContext context;
    private static volatile boolean mustWork = true;

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(Main::shutdown));
        context = new ClassPathXmlApplicationContext("app-context.xml");
        log.info("Spring context loaded.");

    }

    public static void shutdown() {
        log.info("Shutting down...");
        mustWork = false;
        if (context != null) {
            context.close();
        }
    }
}
