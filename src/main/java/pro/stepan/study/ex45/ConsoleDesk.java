package pro.stepan.study.ex45;

import pro.stepan.study.ex45.model.Shape;

public class ConsoleDesk implements Desk {
    @Override
    public void draw(Shape shape) {
        System.out.println(shape);
    }
}
