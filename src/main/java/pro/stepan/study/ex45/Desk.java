package pro.stepan.study.ex45;

import pro.stepan.study.ex45.model.Shape;

public interface Desk {
    void draw(Shape shape);
}
