package pro.stepan.study.ex45;

import lombok.AllArgsConstructor;
import pro.stepan.study.ex45.model.Cannon;
import pro.stepan.study.ex45.model.Circle;
import pro.stepan.study.ex45.model.Point;

@AllArgsConstructor
public class ShootScene {
    private final Desk desk;
    private Cannon cannon;

    public void fire() {
        for (int i = 0; i < 10; i++) {
            Circle bullet = cannon.getBullet().atPosition(new Point(cannon.getBullet().center.x + 50, cannon.getBullet().center.y));
            cannon = cannon.withBullet(bullet);
            desk.draw(cannon);
        }
    }

}
