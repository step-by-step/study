package pro.stepan.study.ex45;

import pro.stepan.study.ex45.model.SimpleCannon;
import pro.stepan.study.ex45.model.Point;

public class Ex45Cannon {

    public static void main(String[] args) {
        Desk desk = new ConsoleDesk();
        SimpleCannon cannon = SimpleCannon.atPosition(new Point(100, 100), 10);
        ShootScene shootScene = new ShootScene(desk, cannon);
        shootScene.fire();
    }
}