package pro.stepan.study.ex45.model;

abstract class AbstractShape implements Shape {
    @Override
    public String toString() {
        return draw();
    }
}