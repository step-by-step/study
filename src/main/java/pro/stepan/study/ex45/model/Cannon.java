package pro.stepan.study.ex45.model;

public interface Cannon extends Shape {
    Circle getBullet();
    Cannon withBullet(Circle bullet);
}