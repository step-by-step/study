package pro.stepan.study.ex45.model;

public class Circle extends AbstractShape implements Movable<Circle> {
    public final Point center;
    public final int radius;

    public Circle(Point center, int radius) {
        this.center = center;
        this.radius = radius;
    }

    @Override
    public String draw() {
        return String.format("Circle[%d,%d] R%d", center.x, center.y, radius);
    }

    @Override
    public Point getPosition() {
        return center;
    }

    @Override
    public Circle atPosition(Point center) {
        return new Circle(center, radius);
    }
}
