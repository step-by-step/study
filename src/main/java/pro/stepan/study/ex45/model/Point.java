package pro.stepan.study.ex45.model;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Point extends AbstractShape implements Movable<Point> {
    public final int x;
    public final int y;

    @Override
    public String draw() {
        return String.format("Point[%d,%d]", x, y);
    }

    @Override
    public Point getPosition() {
        return this;
    }

    @Override
    public Point atPosition(Point center) {
        return center;
    }
}
