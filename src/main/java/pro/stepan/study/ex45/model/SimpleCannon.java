package pro.stepan.study.ex45.model;

import lombok.Getter;

public class SimpleCannon extends AbstractShape implements Cannon {
    public final Rectangle barrel;
    @Getter
    public final Circle bullet;

    private SimpleCannon(Rectangle barrel, Circle bullet) {
        this.barrel = barrel;
        this.bullet = bullet;
    }

    public static SimpleCannon atPosition(Point startPosition, int caliber) {
        Rectangle barrel = new Rectangle(startPosition.x, startPosition.y, 100, caliber + 2);
        return new SimpleCannon(barrel,
                new Circle(barrel.getPosition(), caliber));
    }

    public SimpleCannon withBullet(Circle bullet) {
        return new SimpleCannon(barrel, bullet);
    }

    @Override
    public String draw() {
        return String.format("Cannon: %s %s", barrel, bullet);
    }
}