package pro.stepan.study.ex45.model;

public class Rectangle extends AbstractShape implements Movable<Rectangle> {
    //bottom left x
    public final int x;
    //bottom left y
    public final int y;
    public final int width;
    public final int height;

    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public Rectangle(Point center, int width, int height) {
        this.x = center.x - width / 2;
        this.y = center.y - height / 2;
        this.width = width;
        this.height = height;
    }

    @Override
    public String draw() {
        return String.format("Rectangle[%d,%d] %dx%d", x, y, width, height);
    }

    private Point center = null;

    @Override
    public Point getPosition() {
        if (center == null) {
            center = new Point(x + width / 2, y + height / 2);
        }
        return center;
    }

    @Override
    public Rectangle atPosition(Point center) {
        return new Rectangle(center, width, height);
    }
}