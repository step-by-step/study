package pro.stepan.study.ex45.model;

public interface Shape {
    String draw();
}