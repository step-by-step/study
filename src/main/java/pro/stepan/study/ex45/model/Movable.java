package pro.stepan.study.ex45.model;

public interface Movable<T> {
    Point getPosition();
    T atPosition(Point center);
}